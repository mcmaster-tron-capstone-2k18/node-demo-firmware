# Usage:
# * make clean
# * make all
# * make upload
#
# The following need to be defined:
# ARDUINO_MAKEFILE
# MONITOR_PORT

BOARD_TAG = nano
BOARD_SUB = atmega328
BOARD_CLOCK = 8mhz
ARDUINO_LIBS = arduino-cc1101-master RadioHead SPI

include $(ARDUINO_MAKEFILE)
