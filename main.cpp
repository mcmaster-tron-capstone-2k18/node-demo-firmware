#include <Arduino.h>
#include <HardwareSerial.h>
#include <RH_RF95.h>

#define TX_FREQ (915.0)
#define TX_PWR (6) // dBm
#define TX_TIMEOUT (5000)
#define RX_TIMEOUT (15000)

#define BAUDRATE (115200)

char buf[255];
uint8_t data[512];

#define SS (9)
#define RFM95_INT_PIN (3) // RFM95 Interrupt pin
#define RFM95_RST_PIN (2) // RFM95 Reset pin

RH_RF95 rf95_radio(SS, RFM95_INT_PIN);

int freeRam() {
    extern int __heap_start, *__brkval;
    int v;
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

bool rfm95_easy_init(RH_RF95 *radio, uint8_t reset_pin, RH_RF95::ModemConfigChoice config_choice, float freq, int8_t tx_power) {
    pinMode(RFM95_RST_PIN, OUTPUT);
    digitalWrite(RFM95_RST_PIN, HIGH);
    delay(10);
    digitalWrite(RFM95_RST_PIN, LOW);
    delay(10);
    digitalWrite(RFM95_RST_PIN, HIGH);
    delay(10);

    if (!rf95_radio.init(true))
        return false;

    rf95_radio.setModemConfig(config_choice);
    return true;
}

void setup() {
    Serial.begin(BAUDRATE);
    while (!Serial) ; // Wait for serial port to be available

    Serial.println("Hot Mesh Solutions Node Demo");
    Serial.println("Using RFM95 firmware");
    delay(1000);

    // rf95_radio.Bw125Cr45Sf128 Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range
    // rf95_radio.Bw500Cr45Sf128 Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range
    // rf95_radio.Bw31_25Cr48Sf512 Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range
    // rf95_radio.Bw125Cr48Sf4096 Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, CRC on. Slow+long range
    while (!rfm95_easy_init(&rf95_radio, RFM95_RST_PIN, RH_RF95::Bw125Cr45Sf128, TX_FREQ, TX_PWR)) {
        Serial.println("RFM95 init failed");
        delay(2000);
    }

    rf95_radio.setFrequency(915.0);
    rf95_radio.setTxPower(6, false);
    rf95_radio.printRegisters();
    delay(1000);
}

void loop() {
    Serial.print("Mem free ");
    Serial.println(freeRam());
    memset(data, 50, sizeof(data));
//    sprintf(buf, "Sending %s\r\n", data);
//    Serial.print(buf);
    Serial.println("Sending data");
    rf95_radio.send(data, sizeof(data));

    if (!rf95_radio.waitPacketSent(TX_TIMEOUT)) {
        Serial.println("Timeout");
        return;
    }

    Serial.println("Waiting for reply...");
    if (rf95_radio.waitAvailableTimeout(RX_TIMEOUT)) {
        uint8_t len = sizeof(buf);
        if (rf95_radio.recv(buf, &len)) {
            Serial.print("Got reply: ");
            Serial.println((char*)buf);
            Serial.print("RSSI: ");
            Serial.println(rf95_radio.lastRssi(), DEC);
        }
        else {
            Serial.println("Receive failed");
        }
    }
    else {
        Serial.println("Timeout");
    }
    delay(2000);
}

